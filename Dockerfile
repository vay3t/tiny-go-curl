FROM golang:1.17-alpine AS builder
RUN apk add --no-cache upx gcc musl-dev
WORKDIR /go/src/go-curl
ENV GOOS=linux
ENV GOVERSION=1.17
ENV GO111MODULE=auto
ENV GOPATH=/go
ENV PATH=$GOPATH/bin:$PATH
COPY go-curl.go .
RUN go mod init
RUN go mod tidy
RUN go install -ldflags "-s -w" -trimpath ./...
RUN upx --ultra-brute --lzma -9 --best /go/bin/go-curl

FROM alpine:3.16.0
WORKDIR /usr/bin/
COPY --from=builder /go/bin/go-curl .

ENTRYPOINT ["/usr/bin/go-curl"]
