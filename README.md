# Tiny Go cURL

Little curl in Golang for little mischief - Standalone

## Why Tiny Go?

You don't have an http client and you don't have how to install it (without root) or compile it for the platform you need, this project can help you. Easy to compile, no dependencies and standalone. It has been useful to me in post-exploitation.

## Getting started

### For Linux
```bash
git clone https://gitlab.com/vay3t/tiny-go-curl
cd tiny-go-curl
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-curl.go
upx go-curl
```

### For Windows
```bash
git clone https://gitlab.com/vay3t/tiny-go-curl
cd tiny-go-curl
GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-curl.go
upx go-curl.exe
```

### For MacOS
```bash
git clone https://gitlab.com/vay3t/tiny-go-curl
cd tiny-go-curl
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-curl.go
upx go-curl
```

### For Docker

```bash
git clone https://gitlab.com/vay3t/tiny-go-curl go-curl
cd go-curl
docker build -t go-curl .
```

# Usage

## Help

```bash
Usage of ./go-curl:
  -F value
        Specify multipart MIME data
  -G    Put the post data in the URL and use GET
  -H value
        Pass custom header(s) to server
  -L    Follow redirects
  -X string
        Specify request command to use (default "GET")
  -d string
        HTTP POST data
  -dd value
        HTTP POST data with multiple values
  -dump
        Show request to send
  -h    Show help
  -i    Include protocol response headers in the output
  -k    Allow insecure server connections when using SSL
  -timeout int
        Maximum time allowed for connection (default 10)
  -u string
        url
  -x string
        [protocol://]host[:port] Use this proxy, only supports HTTP/HTTPS

+ Usage: go-curl -u <URL> [OPTIONS]
```

## Examples

* Basic Usage

```bash
./go-curl -u http://example.com
```

* Request with proxy

```bash
./go-curl -u http://example.com -x http://localhost:8080

```

* Basic POST request

```bash
./go-curl -u http://example.com -d "var1=hello&var=world"
```

* Request with custom headers

```bash
./go-curl -u http://example.com -H "User-Agent: Firefox" -H "X-API-Key: ASDASDASD"
```

* Show all headers

```bash
./go-curl -u http://example.com -i
```

* Show request to send

```bash
./go-curl -u http://example.com -dump
```

* Send request with multiples data (POST)

```bash
./go-curl -u http://example.com -dd "key1=value1" -dd "key2=value2"
```

* Send POST request with MIME data

```bash
./go-curl -u http://example.com -F "hola=hola" -F "hola2=hola2" -F "file_image=@archivo.jpg"
```

##### Docker

```bash
docker run -ti --rm go-curl -u https://google.com -L
```
