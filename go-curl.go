package main

/* Cosas por hacer
-w, --write-out <format> Use output FORMAT after completion # https://everything.curl.dev/usingcurl/verbose/writeout
*/

import (
	"bytes"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	urllib "net/url"
	"os"
	"strings"
	"time"
)

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var (
	req    *http.Request
	err    error
	client *http.Client = &http.Client{}
)

func main() {
	var url, method, proxyConfig, data, upload string
	var timeout int
	var header, dinamicData, form arrayFlags
	var include, insecure, location, dumpRequest, help, forceGet, http2 bool

	flag.StringVar(&url, "u", "", "url")

	flag.Var(&header, "H", "Pass custom header(s) to server")
	flag.Var(&dinamicData, "dd", "HTTP POST data with multiple values")
	flag.Var(&form, "F", "Specify multipart MIME data")

	flag.StringVar(&method, "X", "GET", "Specify request command to use")
	flag.StringVar(&data, "d", "", "HTTP POST data")
	flag.StringVar(&proxyConfig, "x", "", "[protocol://]host[:port] Use this proxy, only supports HTTP/HTTPS")
	flag.StringVar(&upload, "T", "", "Transfer local FILE to destination, -T=@<file>")

	flag.IntVar(&timeout, "timeout", 10, "Maximum time allowed for connection")

	flag.BoolVar(&include, "i", false, "Include protocol response headers in the output")
	flag.BoolVar(&insecure, "k", false, "Allow insecure server connections when using SSL")
	flag.BoolVar(&location, "L", false, "Follow redirects")
	flag.BoolVar(&dumpRequest, "dump", false, "Show request to send")
	flag.BoolVar(&forceGet, "G", false, "Put the post data in the URL and use GET")
	flag.BoolVar(&http2, "2", false, "Use HTTP 2")

	flag.BoolVar(&help, "h", false, "Show help")

	flag.Parse()

	client = &http.Client{
		Transport: &http.Transport{
			ForceAttemptHTTP2: http2,
		},
	}

	method = strings.ToUpper(method)

	if url == "" {
		usage()
		os.Exit(1)
	}

	if help {
		usage()
		os.Exit(0)
	}

	if proxyConfig != "" {
		proxyConfiguration(proxyConfig)
	}

	client = &http.Client{
		Timeout: time.Second * time.Duration(timeout),
	}

	if insecure {
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	if !location {
		client = &http.Client{
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}

	}

	if data != "" {
		if forceGet && method != "GET" {
			log.Fatalln("Please use one method: GET or " + method)
		}

		if forceGet {
			method = "GET"
			url = url + "?" + data
		} else if method == "GET" || method == "HEAD" {
			method = "POST"
		}

		req, err = http.NewRequest(method, url, strings.NewReader(data))
		if err != nil {
			log.Fatalln(err)
		}

		if method != "GET" {
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		}
	} else if upload != "" {
		if method == "GET" {
			method = "POST"
		}
		var (
			data []byte
		)

		if upload[0] != '@' {
			log.Fatalln("Please use -T=@<file>")
		}

		data, err = ioutil.ReadFile(upload[1:])
		if err != nil {
			panic(err)
		}
		writer := bytes.NewBuffer(data)
		req, err = http.NewRequest(method, url, writer)
		if err != nil {
			log.Fatal(err)
		}
		req.Header.Set("Content-Type", "application/octet-stream")
	} else {
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			log.Fatalln(err)
		}
	}

	if len(dinamicData) > 0 {
		data_d := dinamicDataParse(&dinamicData)
		data_t := urllib.Values{}
		for k, v := range data_d {
			data_t.Set(k, v)
		}

		if method == "GET" {
			method = "POST"
		}

		req, err = http.NewRequest(method, url, strings.NewReader(data_t.Encode()))
		if err != nil {
			log.Fatalln(err)
		}

		if method == "POST" {
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		}

	}

	if len(form) > 0 {
		if data != "" || len(dinamicData) > 0 {
			log.Fatalln("Please use one option: -F or -d or -dd")
		}
		req = form_creator(url, method, form)
	}

	if len(header) > 0 {
		head := headerParse(&header)
		for k, v := range head {
			req.Header.Set(k, v)
		}
	}

	if dumpRequest {
		dumper(req)
	}

	// ------------------------ Do Request ------------------------
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	if include {
		includer(*resp)
	}

	bodyPrinter(resp)
}

func usage() {
	flag.Usage()
	help := "\n+ Usage: go-curl -u <URL> [OPTIONS]"
	fmt.Println(help)
}

func dumper(req *http.Request) {
	dump, err := httputil.DumpRequest(req, true)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Print(string(dump))
	os.Exit(0)
}

func includer(resp http.Response) {
	b, err := httputil.DumpResponse(&resp, true)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(string(b))
	os.Exit(0)
}

func headerParse(h *arrayFlags) map[string]string {
	header := make(map[string]string)
	for _, h := range *h {
		h := h
		parts := strings.Split(h, ":")
		if len(parts) == 2 {
			header[parts[0]] = parts[1]
		} else {
			log.Fatalln("Invalid header")
		}
	}
	return header
}

func dinamicDataParse(d *arrayFlags) map[string]string {
	listData := make(map[string]string)
	for _, d := range *d {
		d := d
		parts := strings.Split(d, "=")
		if len(parts) == 2 {
			listData[parts[0]] = parts[1]
		} else {
			log.Fatalln("Invalid data")
		}
	}
	return listData
}

func bodyPrinter(resp *http.Response) {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Printf("%s", body)
}

func proxyConfiguration(proxyConfig string) {
	protocol := strings.Split(proxyConfig, "://")[0]
	if protocol == "http" {
		os.Setenv("HTTP_PROXY", proxyConfig)
		os.Setenv("HTTPS_PROXY", proxyConfig)
	} else if protocol == "https" {
		os.Setenv("HTTP_PROXY", proxyConfig)
		os.Setenv("HTTPS_PROXY", proxyConfig)
	} else {
		log.Fatalln("Invalid protocol")
	}
}

func form_creator(url string, method string, formulario arrayFlags) *http.Request {
	var (
		fw   io.Writer
		file *os.File
	)
	// New multipart writer.
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	for _, formulario := range formulario {
		formulario := formulario
		parts := strings.Split(formulario, "=")
		if len(parts) == 2 {
			if parts[1][0] == '@' {
				fw, err = writer.CreateFormFile(parts[0], parts[1][1:])
				if err != nil {
					log.Fatalln(err)
				}
				file, err = os.Open(parts[1][1:])
				if err != nil {
					panic(err)
				}
			} else {
				fw, err = writer.CreateFormField(parts[0])
				if err != nil {
					log.Fatalln(err)
				}
				_, err = io.Copy(fw, strings.NewReader(parts[1]))
				if err != nil {
					log.Fatalln(err)
				}
			}

		} else {
			log.Fatalln("Invalid data")
		}
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		log.Fatalln(err)
	}
	writer.Close()

	if method == "GET" {
		method = "POST"
	}

	// Close multipart writer.
	writer.Close()
	req, err := http.NewRequest(method, url, bytes.NewReader(body.Bytes()))
	if err != nil {
		log.Fatalln(err)
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return req
}

func check_https(url string) bool {
	protocol := strings.Split(url, "://")[0]
	if protocol == "http" {
		return false
	} else if protocol == "https" {
		return true
	} else {
		log.Fatalln("Invalid protocol")
		return false
	}
}
